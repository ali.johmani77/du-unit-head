<?php
$birthdayMessage = "
	Happy Birthday to our exceptional Development Unit Head! 🎉🎂
	Your leadership and guidance have been the driving force behind our team's success.
	May this special day bring you joy, relaxation, and all the recognition you truly deserve.
	Your dedication inspires us all to reach new heights.
	Here's to another year of achievements and milestones under your brilliant leadership! Enjoy your day to the fullest!
\n";
echo $birthdayMessage;

$from = "PHP Developer";
echo $from;
?>
