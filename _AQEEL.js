function generateBirthdayMessage(name) {
    const roleModelMessage = `Happy birthday to my incredible role model, ${name}! 🎉`;
    const favorsMessage = `I want to express my heartfelt gratitude for all the favors you've done for me. Your advice have been invaluable on my journey. Your words still inspire me. Wishing you a day filled with joy, and may this year bring you even greater success and happiness! 🎂🥳`;
  
    const birthdayMessage = `${roleModelMessage}\n\n${favorsMessage}`;
  
    return birthdayMessage;
  }
  
  // Usage:
  const unitHeadName = "PhD. Ubai";
  const birthdayMessage = generateBirthdayMessage(unitHeadName);
  console.log(birthdayMessage);