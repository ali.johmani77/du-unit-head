using System;

class Program
{
    static void Main()
    {
        CelebrateDrUbai();
    }

    static void CelebrateDrUbai()
    {
        Console.WriteLine("Happy Celebration, Dr. Ubai!");
        Console.WriteLine("Wishing you a day filled with joy and success!");
        Console.WriteLine("Thank you for your dedication and hard work.");
        Console.WriteLine("Cheers to more achievements!");
    }
}
